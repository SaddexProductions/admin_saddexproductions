export interface langProps {
    langMenu: boolean;
    closeLangMenu: () => void;
    toggleLangMenu: () => void;
}