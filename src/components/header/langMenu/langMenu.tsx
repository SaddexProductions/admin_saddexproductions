//npm modules
import React from 'react';
import styled, {StyledComponent} from 'styled-components';

//local imports
import { langProps } from './localInterfaces';

//assets
import uk from '../../../assets/uk.png';

//styled components
const LanguageMenu: StyledComponent<"div", any> = styled.div`
    width: 55px;
    z-index: 200;
    cursor: pointer;
    border-radius: 2px;
    overflow: hidden;
    display: inline-table;
    margin-left: 35px;
    transform: translateY(10px);

    img {
        width: 100%;
        margin: 0;
        padding: 0;
    }

    div {
        padding: 0;
        position: absolute;
        height: 30px;
        background-color: #222;
        margin: 0;
        transform: translateY(-4px);
    }

    p {
        font-size: 11px;
        margin: 0;
        display: inline-table;
    }

    @media all and (max-width: 1100px){
        margin-left: 10px;
    }

    @media all and (max-width: 900px){
        display: none;
    }
`;

const langMenu: React.FC <langProps> = props => (
    <LanguageMenu>
        <img src={uk} alt="ukflag" loading="lazy" title="Select language" onClick={(e: React.MouseEvent) => {e.stopPropagation(); props.toggleLangMenu()}}/>
        <div style={{display: props.langMenu ? "inline-table" : "none"}} onClick={props.closeLangMenu}>
            <p>
                Coming soon
            </p>
        </div>
    </LanguageMenu>
);

export default langMenu;