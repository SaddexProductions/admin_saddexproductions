export interface MyProps {
    closeMenu: () => void;
    menuOpen: boolean;
    toggleMenu: () => void;
}

export interface DispatchProps {
    closeLangMenu: () => void;
    toggleLangMenu: () => void;
    setSize: () => void;
}

export interface StateProps {
    isAuth: boolean;
    langMenu: boolean;
}