//npm modules
import React from 'react';
import styled, {StyledComponent} from 'styled-components';
import {NavLink} from 'react-router-dom';

//styled components
const OuterWrapper: StyledComponent<"div", any> = styled.div`
    width: auto;
    display: inline-table;
    margin: auto;

    @media all and (max-width: 900px){
        display: none;
    }
`;

const MenuWrapper: StyledComponent<"div", any> = styled.div`
    justify-content: space-between;
    display: flex;
    flex-direction: row;
    width: 100%;
    overflow-y: hidden;
    a {
        text-decoration: none;
        color: #fff;
        outline: none;
    }
    a.active, a:hover, a:focus {
        border-bottom: 3px solid #ffd400;
        background-color: #333;
        cursor: pointer;
        outline: none;
    }
`;

const MenuItem: StyledComponent<"h2", any> = styled.h2`
    display: inline-block;
    color: #fff;
    font-size: 22px;
    box-sizing: border-box;
    white-space: nowrap;
    text-align: center;
    padding: 0;
    height: 36.5px;
    width: 115px;

    @media all and (max-width: 1100px){
        padding: 9.9px 0px;
        width: 94px;
        height: auto;
        font-size: 18.5px;
    }
`;

const menu: React.FC = () => (
    <OuterWrapper data-testid="horizontalMenu">
        <MenuWrapper>
            <NavLink to="/" exact><MenuItem>Pages</MenuItem></NavLink>
            <NavLink to="/items"><MenuItem>Items</MenuItem></NavLink>
        </MenuWrapper>
    </OuterWrapper>
);

export default menu;