//npm modules
import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup} from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import testrender from 'react-test-renderer';

//local imports
import {TestHeader} from './header';


describe('Header component', () => {

    afterEach(cleanup);

    it("should render without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<BrowserRouter><TestHeader/></BrowserRouter>, div);
    });

    it('should contain horizontal menu when isAuth is true', () => {
        const {getByTestId} = render(<BrowserRouter><TestHeader isAuth={true}></TestHeader></BrowserRouter>);
        expect(getByTestId('innerHeader').innerHTML).toMatch(/data-testid=\"horMenu\"/);
    });

    it('should not contain horizontal menu when isAuth is false', () => {
        const {getByTestId} = render(<BrowserRouter><TestHeader isAuth={false}></TestHeader></BrowserRouter>);
        expect(getByTestId('innerHeader').innerHTML).not.toMatch(/data-testid=\"horMenu\"/);
    });

    it("should match snapshot", () => {
        const instance = testrender.create(<BrowserRouter><TestHeader/></BrowserRouter>);
        expect(instance).toMatchSnapshot();
    });
});