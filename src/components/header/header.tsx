//npm modules
import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import styled, {StyledComponent} from 'styled-components';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

//local imports
import * as actions from '../../store/actions/index';
import Logo from '../logo/logo';
import Menu from './menu/menu';
import Nav from '../UI/navbar/navButton/navButton';
import NavMenu from '../UI/navbar/navMenu/navMenu';
import LangMenu from './langMenu/langMenu';
import { StateProps, DispatchProps, MyProps } from './localInterfaces';

//styled components
const HeaderComp: StyledComponent<"div", any, {}, never> = styled.div`
    background-color: #000;
    height: 75px;
    border-bottom: 3px solid #fff;
    display: block;
    box-sizing: border-box;
    position:fixed;
    width: 100%;
    z-index: 52;

    .innerHeader {
        width: calc(98% - 12px);
        margin: auto auto auto 1%;
        display: flex;
        justify-content: space-between;
        box-sizing: border-box;
    }

    @media all and (max-width: 900px){
        .innerHeader {
            width: 100%;
            margin: 0;
            display: block;
        }
    }
`;

const LogoContainer: StyledComponent<"div", any, {}, never> = styled.div`
    margin-top: 10px;
    margin-right: 25px;
    display: inline-block;
    transition: filter 0.4s ease;

    :hover {
        filter: drop-shadow(0 0 5px #fff);
    }

    a {
        outline: none
    }

    @media all and (max-width: 1100px){
        margin-right: 12px
    }

    @media all and (max-width: 900px){
        margin: 10px 20px;
    }
`;

const Header: React.FC<DispatchProps & StateProps & MyProps> = props => (
    <Fragment>
        <HeaderComp>
            <div className="innerHeader" data-testid="innerHeader">
                <LogoContainer>
                    <NavLink to="/"><Logo width={1} /></NavLink>
                </LogoContainer>
                <Nav clicked={props.toggleMenu} />
                {props.isAuth ? (<div data-testid="horMenu">
                    <Menu />
                    <LangMenu {...props} />
                </div>) : <h2>Admin dashboard</h2>}
            </div>
        </HeaderComp>
        <NavMenu active={props.menuOpen} clicked={props.closeMenu} toggleLangMenu={props.toggleLangMenu} langMenu={props.langMenu}/>
    </Fragment>
);

export const TestHeader: React.FC<any> = Header;

const mapStateToProps = (state: any) => {

    const obj: StateProps = {
        isAuth: state.auth.token != null,
        langMenu: state.globalUI.langMenu
    };
    return obj;
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    const obj: DispatchProps = {
        setSize: () => dispatch(actions.setSize()),
        toggleLangMenu: () => dispatch(actions.toggleLangMenu()),
        closeLangMenu: () => dispatch(actions.closeLangMenu())
    }

    return obj;
}

export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(Header);