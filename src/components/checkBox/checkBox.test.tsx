import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup} from '@testing-library/react';
import testrender from 'react-test-renderer';

//local/component to be tested
import CheckBox from './checkBox';

const mock = jest.fn();

describe('checkBox component', () => {

    afterEach(cleanup);

    const standard = <CheckBox toggle={mock} checked={false}/>;

    it('should render without crashing', () => {
        const div = document.createElement("div");
        ReactDOM.render(standard, div)
    });

    it('should render the checkmark if prop checked is true', () => {
        const {getByTestId} = render(<CheckBox toggle={mock} checked={true}/>);
        expect(getByTestId('checkbox').innerHTML).toMatch(/data-testid=\"checkmark\"/);
    });

    it('should not render the checkmark if prop checked is false', () => {
        const {getByTestId} = render(<CheckBox toggle={mock} checked={false}/>);
        expect(getByTestId('checkbox').innerHTML).not.toMatch(/data-testid=\"checkmark\"/);
    });

    it("should match snapshot", () => {
        const instance = testrender.create(standard);
        expect(instance).toMatchSnapshot();
    });
});