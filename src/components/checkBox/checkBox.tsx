//npm modules
import React, { Fragment } from 'react';
import styled, { StyledComponent } from 'styled-components';

//styled components
const CheckBoxDiv: StyledComponent<"div", any> = styled.div`
    width: 25px;
    height: 25px;
    cursor: pointer;
    background-color: #fff;
    display: inline-block;
    .checkmark {
        width: 7px;
        pointer-events: none;
        background-color: #071;
        height: 30px;
        margin-left: 13px;
        transform-origin: center center;
        transform: rotate(45deg);
    }
    .checkmark:after {
        content: "";
        width: 100%;
        height: 40%;
        background-color: #071;
        transform: rotate(90deg) translate(21px, 5px);
        display: block;
    }
}
&.disabled {
    background-color: #ccc;
    .checkmark:after {
        background-color: rgb(91, 111, 94)
    }
}
`;

const checkBox: React.FC<{checked: boolean; toggle: () => void}> = props => {

    return (
        <Fragment>
            <CheckBoxDiv data-testid="checkbox" onClick={props.toggle}>
                {props.checked && <div className="checkmark" data-testid="checkmark"/>}
            </CheckBoxDiv>
        </Fragment>
    );
}

export default checkBox;