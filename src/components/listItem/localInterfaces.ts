export interface ListItemInterface {
    item: {
        title?: string;
        name?: string;
        url: string;
        image?: string;
        thumb?: string;
        frontEnd?: string;
        backEnd?: string;
        [key: string]: string | undefined;
    }
}