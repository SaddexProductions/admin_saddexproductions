//npm modules
import React from 'react';

//local imports
import styled, { StyledComponent } from 'styled-components';
import { ListItemInterface } from './localInterfaces';

const ListItem: StyledComponent<"div", any> = styled.div`
    width: 100%;
    padding: 3px 10px;
    box-sizing: border-box;
    background-color: #fff;
    border-bottom: 2px solid #000;
    height: 70px;
    margin: 0;

    p, h3 {
        color: #000;
    }

    strong {
        text-transform: capitalize;
    }

    h3 {
        margin: 3px 0;
    }

    p {
        margin: 2px 0;
        font-size: 12px;
    }
`;

const listItemComp: React.FC<ListItemInterface> = props => (
    <ListItem>
        {Object.keys(props.item).map((key: string) => {
            switch(key){
                case "title":
                    return <h3>{props.item[key]}</h3>;
                case "name":
                    return <h3>{props.item[key]}</h3>
                default:
                    return <p><strong>{key}</strong>: {props.item[key]}</p>
            }
        })}
    </ListItem>
);

export default listItemComp;