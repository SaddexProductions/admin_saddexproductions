//npm modules
import React from 'react';
import styled, {StyledComponent} from 'styled-components';

//assets
import icon from '../../../../assets/navicon.png';

//styled components
const Icon: StyledComponent<"img", any> = styled.img`
    display: none;

    @media all and (max-width: 900px){
        margin-top: 12px;
        height: 38px;
        width: 38px;
        margin-left: 0;
        padding: 5px 24px;
        display: inline-table;
        background-color: #fff;
        position: relative;
        border-radius: 5px;
        cursor: pointer;
    }
`;

const menuButton: React.FC <{clicked: (e: React.MouseEvent) => void}> = props => (
    <Icon src={icon} onClick={props.clicked} alt="Menubutton"/>
);

export default menuButton;