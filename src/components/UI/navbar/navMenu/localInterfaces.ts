export interface NavMenuProps {
    active: boolean;
    clicked: (e: React.MouseEvent) => void;
    langMenu: boolean;
    toggleLangMenu: () => void;
}