//npm modules
import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup} from '@testing-library/react';
import testrender from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';

//local imports
import NavMenu from './navMenu';

describe('Navmenu component', () => {

    afterEach(cleanup);

    const standard: JSX.Element = <BrowserRouter><NavMenu 
    active={false} 
    clicked={jest.fn()} 
    langMenu={false} 
    toggleLangMenu={jest.fn()}/></BrowserRouter>;

    it('should render without crashing', () => {
        const div = document.createElement("div");
        ReactDOM.render(standard, div);
    });

    it('should not have active class if props.active set to false', () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('menuContainer')).not.toHaveClass("active");
    });

    it('should have class active if props.active set to true', () => {
        const {getByTestId} = render(<BrowserRouter><NavMenu active={true} clicked={jest.fn()} langMenu={false} toggleLangMenu={jest.fn()}/></BrowserRouter>);
        expect(getByTestId('menuContainer')).toHaveClass("active");
    });

    it('should not display the language menu if props.langMenu set to false', () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('navLangMenu')).toHaveStyle("display: none");
    });

    it('should display the language menu if props.langMenu set to true', () => {
        const {getByTestId} = render(<BrowserRouter><NavMenu active={true} clicked={jest.fn()} langMenu={true} toggleLangMenu={jest.fn()}/></BrowserRouter>);
        expect(getByTestId('navLangMenu')).toHaveStyle("display: inline");
    });

    it('should match snapshot', () => {
        const instance = testrender.create(standard);
        expect(instance).toMatchSnapshot();
    });
});

