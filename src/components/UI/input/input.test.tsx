//npm modules
import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup, fireEvent} from '@testing-library/react';
import testrender from 'react-test-renderer';

//local imports
import Input from './input';
import { Field, Email } from '../../../containers/auth/localClasses';

//setup
const field = new Field(new Email());
field.elementType = "input"; //redundant but makes test code easier to understand
const options = [{value: "sample", displayValue: "Sample"}];

const emailInput = (<Input
    {...field}
    changed={jest.fn()} 
    keyup={jest.fn()}
    invalid={!field.valid}
    id={field.name}
    />);

describe('input component', () => {

    afterEach(cleanup);

    it('should render without crashing', () => {
        const div = document.createElement("div");
        ReactDOM.render(emailInput, div);
    });

    it('should render a regular input when props.elementType is input and props.elementConfig.type is not checkbox', () => {

        const {getByTestId} = render(emailInput);
        expect(getByTestId('inputComponent').innerHTML).toMatch(/data-testid=\"input-input\"/);
    });

    it('should contain custom checkbox when props.elementType is input and props.elementConfig.type is checkbox', () => {
        const {getByTestId} = render(<Input 
            {...field} 
            elementConfig={{type: "checkbox"}} 
            invalid={false} 
            id={field.name} 
            changed={jest.fn()}
            keyup={jest.fn()}
            />);
        expect(getByTestId('inputComponent').innerHTML).toMatch(/data-testid=\"checkbox\"/);
    });


    it('should render a textarea when props.elementType is textarea', () => {
        const {getByTestId} = render(<Input 
            {...field} 
            changed={jest.fn()} 
            keyup={jest.fn()} 
            invalid={!field.valid} 
            id={field.name} 
            elementType="textarea"/>);
            expect(getByTestId('inputComponent').innerHTML).toMatch(/data-testid=\"input-textarea\"/);
    });

    it('should render validation error description via <Status> tag when props.desc is truthy, input has been unfocused once and props.invalid is true',
    () => {
        const {getByTestId} = render(<Input
            {...field}
            changed={jest.fn()}
            keyup={jest.fn()}
            invalid
            id={field.name}
            desc="Generic error"
            />)
        const inputEl = getByTestId('input-input') as HTMLInputElement;
        fireEvent.blur(inputEl);
        expect(getByTestId('inputComponent').innerHTML).toMatch(/data-testid=\"statusText\"/);
    });

    it('should not render validation error description via <Status> tag when props.desc is truthy, props.invalid is true but input hasnt been unfocused',
    () => {
        const {getByTestId} = render(<Input
            {...field}
            changed={jest.fn()}
            keyup={jest.fn()}
            invalid
            id={field.name}
            desc="Generic error"
            />);
        expect(getByTestId('inputComponent').innerHTML).not.toMatch(/data-testid=\"statusText\"/);
    });

    it('should not render validation error description via <Status> tag when props.desc is falsy, despite props.invalid being true and element has been unfocused',
    () => {
        const {getByTestId} = render(<Input
            {...field}
            changed={jest.fn()}
            keyup={jest.fn()}
            invalid
            id={field.name}
            />);
        const inputEl = getByTestId('input-input') as HTMLInputElement;
        fireEvent.blur(inputEl);
        expect(getByTestId('inputComponent').innerHTML).not.toMatch(/data-testid=\"statusText\"/);
    });

    it('should not render validation error description via <Status> tag when props.invalid is false, despite props.desc being truthy and element has been unfocused',
    () => {
        const {getByTestId} = render(<Input
            {...field}
            changed={jest.fn()}
            keyup={jest.fn()}
            invalid={false}
            id={field.name}
            />);
        const inputEl = getByTestId('input-input') as HTMLInputElement;
        fireEvent.blur(inputEl);
        expect(getByTestId('inputComponent').innerHTML).not.toMatch(/data-testid=\"statusText\"/);
    });

    it('should render a select when props.elementType is select', () => {
        const {getByTestId} = render(<Input 
            {...field} 
            changed={jest.fn()} 
            keyup={jest.fn()} 
            invalid={!field.valid} 
            id={field.name}
            elementConfig={
                {
                    ...field.elementConfig,
                    options
                }
            }
            elementType="select"/>);
            expect(getByTestId('inputComponent').innerHTML).toMatch(/data-testid=\"input-select\"/);
    });

    it('should render two option elements when options array contains two options and elementType is select', () => {
        const customOptions = [...options, {value: "sample2", displayValue: "Sample 2"}];

        const {getAllByTestId} = render(<Input 
            {...field} 
            changed={jest.fn()} 
            keyup={jest.fn()} 
            invalid={!field.valid} 
            id={field.name}
            elementConfig={
                {
                    ...field.elementConfig,
                    options: customOptions
                }
            }
            elementType="select"/>);
            expect(getAllByTestId('input-select-option')).toHaveLength(2);
    });

    it('should have the headline "repeat Password" to props.name = "repeat-Password', () => {
        const customName = {...field, name: "repeat-Password"}
        const {getByTestId} = render(<Input 
            {...customName} 
            invalid={false} 
            id={customName.name} 
            changed={jest.fn()}
            keyup={jest.fn()}
            />);
        expect(getByTestId('inputComponentLabel').textContent).toEqual("repeat Password");
    });

    it('should match snapshot', () => {
        const instance = testrender.create(emailInput);
        expect(instance).toMatchSnapshot();
    });

});