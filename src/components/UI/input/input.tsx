//npm modules
import React, { useState } from 'react';
import styled, { StyledComponent } from 'styled-components';

//local imports
import {InputProps} from './localInterfaces';
import CheckBox from '../../checkBox/checkBox';
import Status from '../status/status';

//styled components
const Input: StyledComponent<"div", any> = styled.div`
    padding: 0;
    display: block;
    margin: 25px auto 4px auto;
    box-sizing:border-box;
    input,textarea,select {
        outline:none;
        border:1px solid #ccc;
        background-color:white;
        font:inherit;
        padding:6px 10px;
        display:block;
        width: calc(100% - 22px);
    }
    input:focus, textarea:focus, select:focus {
        outline:none;
        background-color:white;
    }
    .invalid {
        border: 1px solid #f00;
        background-color: #FDA49A;
    }

    .inputHead {
        text-transform: capitalize;
        margin-bottom: 6px;
        display: inline-block;
        font-weight: bold;
    }

    .adjustedLabel {
        position: absolute;
        margin-top: 4px;
        cursor: pointer;
        padding-left: 10px;
    }
`;

const InputComp: React.FC<InputProps> = props => {

    let inputElement: null | JSX.Element = null;
    let inputClasses: undefined | string;

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses = "invalid";
    }

    //useState
    const [focused, setFocused] = useState(false);

    //handlers

    const toggleCheckbox = () => {
        props.changed({
            target: {
                id: props.id,
                value: !props.value
            }
        })
    }

    switch (props.elementType) {
        case ('input'):
            if(props.elementConfig.type !== "checkbox") {
                inputElement = <input
                data-testid="input-input" 
                {...props.elementConfig} 
                value={props.value} 
                onChange={props.changed} 
                className={inputClasses} 
                aria-label={props.name} 
                id={props.id}
                onBlur={() => setFocused(true)}
                />
            } else {

                inputElement = <CheckBox toggle={toggleCheckbox} checked={props.value}/>;
            }
            break;
        case ('textarea'):
            inputElement = <textarea {...props.elementConfig}
                data-testid="input-textarea" 
                value={props.value}
                aria-label={props.name}
                onChange={props.changed}
                className={inputClasses}
                onKeyUp={props.keyup}
                id={props.id}
                onBlur={() => setFocused(true)}
                />
            break;
        case ('select'):
            inputElement = (<select
                data-testid="input-select"
                {...props.elementConfig}
                value={props.value} onChange={props.changed}
                aria-label={props.name}
                id={props.id}
                onBlur={() => setFocused(true)}
                className={inputClasses}>
                {props.elementConfig.options!.map(option => (
                    <option data-testid="input-select-option" key={option.value} value={option.value}>{option.displayValue}</option>
                ))}
            </select>);
            break;
        default:
            inputElement = <input 
            {...props.elementConfig} 
            value={props.value} 
            onChange={props.changed} 
            className={inputClasses} 
            aria-label={props.name} 
            id={props.id}
            onBlur={() => setFocused(true)}
            />
    }

    let name = props.name;

    if(name.indexOf("-") >-1) {
        name = name.split("-").join(" ");
    }

    return (
        <Input key="container" data-testid="inputComponent">
            {props.elementConfig.type !== "checkbox" && <label
            htmlFor={props.id} 
            className="inputHead" 
            data-testid="inputComponentLabel">{name}</label>}
            {inputElement}
            {props.desc && props.invalid && focused ? <Status 
            form={{error: props.invalid && !!props.desc, message: props.desc || ""}}
            mT/>: null}
            {props.elementConfig.type === 'checkbox' && <label 
            htmlFor={props.id}
            data-testid="inputComponentLabel" 
            className="inputHead adjustedLabel"
            onClick={toggleCheckbox}
            >Save this device as trusted</label> }
        </Input>
    );
}

export default InputComp;