//npm modules
import {KeyboardEvent} from 'react';

//local imports
import { Config, CustomEvent } from '../../../shared/constants';

export interface InputProps {
    changed: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> | CustomEvent) => void;
    keyup: (e: KeyboardEvent<HTMLTextAreaElement>) => void;
    elementConfig: Config;
    shouldValidate: boolean;
    touched: boolean;
    invalid: boolean;
    value: any;
    desc?: string;
    id: string;
    name: string;
    elementType: string;
    toggle?: () => void;
}
