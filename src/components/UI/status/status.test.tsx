//npm modules
import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup} from '@testing-library/react';
import testrender from 'react-test-renderer';

//local imports
import Status from './status';

const form = {
    error: false,
    content: {},
    isValid: true,
    message: "Displaying without error"
}

const standard = <Status form={form}/>;

describe('Status component', () => {

    afterAll(cleanup);

    it('should render without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(standard, div);
    });

    it('should have class confirm if props.form.error is false', () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('statusText')).toHaveClass('confirm');
    });

    it('should not have class confirm if props.form.error is true', () => {
        const changedForm = {...form, error: true};
        const {getByTestId} = render(<Status form={changedForm}/>);
        expect(getByTestId('statusText')).not.toHaveClass('confirm');
    });

    it('should have both classes confirm and noFloat-mt if props.form.error is true and props.noFloat is true', () => {
        const {getByTestId} = render(<Status form={form} mT/>);
        expect(getByTestId('statusText')).toHaveClass('confirm mt');
    });

    it('should show a checkmark fontawesome icon if props.form.error is false', () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('statusText').innerHTML).toMatch(/fa-check-circle/);
    });

    it('should show a exclamation triangle fontawesome icon if props.form.error is true', () => {
        const changedForm = {...form, error: true};
        const {getByTestId} = render(<Status form={changedForm}/>);
        expect(getByTestId('statusText').innerHTML).toMatch(/fa-exclamation-triangle/);
    });

    it('should match snapshot', () => {
        const instance = testrender.create(standard);
        expect(instance).toMatchSnapshot();
    });
});