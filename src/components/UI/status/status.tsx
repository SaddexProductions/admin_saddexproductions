//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

//local imports
import { StatusProps } from './localInterfaces';

//styled components
const Text: StyledComponent<"p", any> = styled.p`
    color: #990000;
    font-weight: bold;
    margin: 0;
    font-size: 13.5px;

    &.confirm {
        color: #0f693c;
    }

    &.mt {
        margin-top: 4px;
    }
`;

const Status: React.FC<StatusProps> = props => {

    const classes: string[] = [];

    if(!props.form.error) {
        classes.push("confirm");
    }

    if(props.mT) {
        classes.push("mt");
    }

    return (
        <Text className={classes.join(" ")} data-testid="statusText">
            <FontAwesomeIcon icon={props.form.error ? faExclamationTriangle : faCheckCircle} />
            {` ${props.form.message}`}
        </Text>
    );
};


export default Status;