//local imports
import { Ce } from "../../../shared/constants";

export interface StatusProps {
    form: {
        message: string;
        error: boolean | Ce;
    };
    mT?: boolean;
}