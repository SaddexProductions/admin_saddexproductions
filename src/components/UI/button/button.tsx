//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

//local imports
import {ButtonProps} from './localInterfaces';

//styled components
const Button: StyledComponent<"button", any> = styled.button`
    display: block;
    color: #fff;
    background-color: #000;
    position: relative;
    cursor: pointer;
    box-sizing: border-box;
    outline: 0;
    padding: 10px 1%;
    width: 200px;
    font-size: 23px;
    font-weight: bold;
    border-radius: 5px;
    text-decoration: none;
    border: none;
    transition: all 0.4s ease;

    :hover, :focus {
        color: #000;
        background-color: #fff;
        outline: 0;
    }

    :disabled {
        color: #ddd;
        background-color: #666;
        cursor: not-allowed;
    }

    @media all and (max-width: 600px){
        width: 160px;
        font-size: 18px;
    }
    @media all and (max-width: 410px){
        width: 115px;
    }
`;

const button: React.FC<ButtonProps> = props => (
    <Button 
    onClick={props.clicked} 
    style={props.passStyle}
    type={props.type}
    disabled={props.disabled}
    aria-label={props.name}
    name={props.name}
    >
        {props.children}
    </Button>
);

export default button;