//npm modules
import {MouseEvent} from 'react';

export interface ButtonProps {
    clicked?: (e: MouseEvent) => void;
    passStyle?: {[key: string]: string};
    type: "button" | "reset" | "submit"
    name: string;
    disabled: boolean;
}