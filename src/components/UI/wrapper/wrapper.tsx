//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

//styled components
const WrapperElement: StyledComponent<"div", any> = styled.div`
    display: inline-block;
    width: 100%;
    margin: 0;
    top: 80px;
    position: relative;
`;

const Wrapper: React.FC = (props: any) => (
    <WrapperElement className="wrapper">
        {props.children}
    </WrapperElement>
);

export default Wrapper;