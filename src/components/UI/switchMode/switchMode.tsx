//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

//styled components
const SmallText: StyledComponent<"small", any> = styled.small`
    color: #fff;
    display: block;
    padding: 3px 0;
    font-weight: bold;
    text-align: center;
    cursor: pointer;
`;

const switchMode: React.FC<{clicked?: (e: React.MouseEvent) => void}> = props => (
    <SmallText data-testid="smalltext" onClick={props.clicked}>{props.children}</SmallText>
);

export default switchMode;