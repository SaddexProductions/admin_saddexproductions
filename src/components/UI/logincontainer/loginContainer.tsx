//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

//styled components
const Cont: StyledComponent<"div", any> = styled.div`
    margin: 80px auto 90px auto;
    border-radius: 10px;
    background-color: #9a9a9a;
    display: block;
    position: relative;
    width: 350px;
`;

const LoginBox: React.FC = props => {
    return (
        <Cont>
            {props.children}
        </Cont>
    )
}

export default LoginBox;