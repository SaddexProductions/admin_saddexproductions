//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';

//styled components
const H1: StyledComponent<"h1", any> = styled.h1`
    display: block;
    margin: auto;
    text-align: center;
    font-size: 40px;
`;

const Headline: React.FC = props => (
    <H1>{props.children}</H1>
);

export default Headline;