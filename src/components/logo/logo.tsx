//npm modules
import React from 'react';

const logo: React.FC <{width: number}> = props => (
    <img src="/img/saddexprods.png" style={{width: (110*props.width).toString() + "px"}} alt="Logo"/>
);

export default logo;