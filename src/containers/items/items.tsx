//npm modules
import React from 'react';
import styled, { StyledComponent } from 'styled-components';
import { withRouter } from 'react-router-dom';
import ListItemComp from '../../components/listItem/listItem';

//styled components
const MainRow: StyledComponent<"div", any> = styled.div`
    width: 60%;
    background-color: #fff;
    height: calc(100vh - 75px);

    .bar {
        min-height: 80px;
        background-color: #bbb;
    }
`;

const SideRow: StyledComponent<"div", any> = styled.div`
    width: 40%;
    background-color: #aaa;
    height: calc(100vh - 75px);
`;

const exampleArray = [
    {
        title: "Example 1",
        url: "example.com"
    },
    {
        title: "Example 2",
        url: "exampletwo.com"
    }
];

const Items: React.FC = (props: any) => {

    return (
        <div style={{top: "75px", position: "relative", display: "flex"}}>
            <MainRow>
                <div className="bar">
                </div>
                {exampleArray.map(m => <ListItemComp item={m}/>)}
            </MainRow>
            <SideRow>

            </SideRow>
        </div>
    )
}

export default withRouter(Items);