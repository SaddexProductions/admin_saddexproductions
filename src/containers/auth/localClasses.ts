import {BaseLoginFormStructure, FieldStructure} from './localInterfaces';
import { Ce, Config, RulesInterface } from '../../shared/constants';

export class FormObject implements BaseLoginFormStructure {
    isValid: boolean = false;
    error: Ce | boolean = false;

    constructor(public content: { [key: string]: FieldStructure }, public message: string = "") { }
}

export class Field implements FieldStructure {
    value = "";
    valid = false;
    touched = false;
    elementConfig: Config;
    name: string;
    rules: RulesInterface;
    elementType: string = "input";
    expand?: boolean;
    offset?: number;
    trim?: boolean;
    desc?: string;

    constructor(
        inputClass: {
            elementConfig: Config, 
            name: string, 
            elementType: string, 
            rules: RulesInterface, 
            expand?: boolean, 
            offset?: number,
            desc?: string
        },
        public shouldValidate: boolean = true) {

            this.rules = inputClass.rules;
            this.elementConfig = inputClass.elementConfig;
            this.name = inputClass.name;
            this.elementType = inputClass.elementType;
            this.expand = inputClass.expand;
            this.offset = inputClass.offset;
            this.desc = inputClass.desc;
        }
}

export class Email {
    rules: RulesInterface = {
        isEmail: true,
        required: true
    };
    elementConfig: Config = {
        type: "email",
        placeholder: "example@gmail.com"
    }
    elementType: string = "input";
    name: string = "email";
}

export class Password {
    rules: RulesInterface = {
        required: true
    };
    elementConfig: Config = {
        type: "password",
        placeholder: "5-25 characters"
    };
    elementType: string = "input";
    name: string = "password";
    desc?: string;

    constructor(withExtraValidation: boolean = false) {
        if(withExtraValidation) {
            this.rules = {
                ...this.rules,
                minLength: 5,
                maxLength: 25,
                regex: /^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{5,25}$/
            }
        }
    }
}

export class RepeatPassword extends Password {

    rules: RulesInterface = {
        required: true,
        mustMatch: true
    }

    name: string = "repeat-Password";
    desc: string = "The passwords do not match";

    constructor() {
        super(false); //Should always be set to false since the original Password class validation has been overridden
        this.elementConfig.placeholder = "Passwords must match";
    }
}

export class Token {
    rules: RulesInterface = {
        required: true,
        minLength: 7,
        isNumeric: true,
        trim: true
    };
    elementConfig: Config = {
        type: "text",
        maxLength: 9,
        placeholder: "Enter token, example 12 34 56 7"
    }
    elementType: string = "input";
    name: string = "token";
    expand: boolean = true;
    offset: number = 2;
}
