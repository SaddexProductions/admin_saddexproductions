import React, { useState, ChangeEvent, KeyboardEvent, FormEvent, Fragment, useEffect } from 'react';
import styled, { StyledComponent } from 'styled-components';
import { connect } from 'react-redux';
import {NavLink, withRouter, useHistory} from 'react-router-dom';
import { Dispatch } from 'redux';

//local imports
import * as actions from '../../store/actions/index';
import LoginContainer from '../../components/UI/logincontainer/loginContainer';
import { Field, FormObject, Email, Password, Token, RepeatPassword } from './localClasses';
import Input from '../../components/UI/input/input';
import Wrapper from '../../components/UI/wrapper/wrapper';
import Button from '../../components/UI/button/button';
import {AuthProps} from './localInterfaces';
import { convertError, updateForm } from '../../shared/utility';
import Spinner from '../../components/UI/spinner/spinner';
import Status from '../../components/UI/status/status';
import SwitchMode from '../../components/UI/switchMode/switchMode';
import { CustomEvent } from '../../shared/constants';

//styled components

const H1: StyledComponent<"h1", any> = styled.h1`
    color: #000;
    display: inline-block;
    margin: 20px 0 0 0;
`;

const Auth: React.FC<AuthProps> = props => {

    //history
    const history = useHistory();

    //state

    const [signin, setSignin] = useState(new FormObject({
        email: new Field(
            new Email()
        ),
        password: new Field(
            new Password()
        )

    }));

    const [forgot, setForgot] = useState(new FormObject({
        email: new Field(new Email())
    }));

    const [twoFA, setTwoFA] = useState(new FormObject({
        token: new Field(new Token()),
        save: new Field({
            elementConfig: {
                type: "checkbox"
            },
            rules: {required: false},
            name: "save",
            elementType: "input"
        }, false)
    }));

    const [resetPW, setResetPW] = useState(new FormObject({
        password: new Field({...new Password(true), 
            desc: "Must contain at least one lowercase letter, one uppercase letter and one number"
        }),
        "repeat-Password": new Field(new RepeatPassword())

    }))

    const [mode, setMode] = useState<"signin" | "forgot" | "2fa" | "reset">("signin");

    const [resetToken, setResetToken] = useState<string>("");

    //variables
    let elementToDisplay: JSX.Element | null = null;
    let obj: FormObject = signin;
    let setObj = setSignin;
    let headLine: string = "Sign in";
    let sendHandler: () => void = () => {props.auth(signin.content.email.value.toString(), 
        signin.content.password.value.toString())};
    let buttonText: string = "Sign in";
    let switchText: string = "Forgot password?";
    let forgotSwitch: string = "#forgotpassword";

    //constants
    const {clearError, error, loading, notice, resetNotice, twoFactorToken} = props;

    //useEffect
    useEffect(() => {
        if(error && error !== obj.error) {
            setObj({...obj, error, message: convertError(error)});
            clearError();
        }
        else if (!error && obj.error && loading){
            setObj({...obj, error: false, message: ""});
        }
        else if (!error && notice && !obj.message) {
            setObj({...obj, error: false, message: notice});

            clearError();

            if(mode === "forgot" || mode === "reset") {
                const copyObjContent = {...obj.content};
                for(const key in copyObjContent) {
                    copyObjContent[key] = {
                        ...copyObjContent[key],
                        value: "",
                        valid: false,
                        touched: false
                    }
                }

                if(mode === "reset") setResetToken("");

                setObj({...obj, isValid: false, content: {...copyObjContent}, error: false, message: notice});

                setTimeout(() => {
                    resetNotice(); 
                    history.push('/login');
                    setObj({...obj, message: ""});
                }, 3500);
            }
        }
    }, [error, setObj, obj, notice, history, mode, loading, resetNotice, clearError]);

    useEffect(() => {
        if(props.history.location && props.history.location.hash) {
            switch(props.history.location.hash) {
                case "#forgotpassword":
                    setMode("forgot");
                    break;
                case "#reset":
                    let cookie: string = document.cookie;
                    if(cookie.indexOf("reset") < 0) {
                        props.history.push('/login');
                    } else {
                        let cook: string = "";
                        let value: string = "; " + cookie;
                        let parts: string[] = value.split("; reset=");
                        if (parts.length > 1) cook = parts[parts.length-1].split(";")[0];
                        setResetToken(cook);
                        setMode("reset");
                    }
                    break;
                default:
                    setMode("signin");
            }
        }
        else {
            if(twoFactorToken) {
                setMode("2fa");
            }
            else {
                setMode('signin');
            }
        }
    }, [props.history, props.history.location, twoFactorToken]);

    //switches, modals
    switch(mode) {
        case "forgot":
            headLine = "Forgot Password";
            buttonText = "Submit";
            switchText = "Cancel";
            forgotSwitch = "/login";
            setObj = setForgot;
            sendHandler = () => props.forgotPassword(obj.content.email.value.toString());
            obj = forgot;
            break;
        case "reset":
            headLine = "Reset Password";
            buttonText = "Reset password";
            obj = resetPW;
            setObj = setResetPW;
            sendHandler = () => props.resetPassword(obj.content.password.value.toString(), 
            obj.content["repeat-Password"].value.toString(), resetToken);
            break;
        case "2fa":
            headLine = "Two-Factor Authentication";
            buttonText = "Submit";
            switchText = "Want the token per SMS instead?";
            obj = twoFA;
            setObj = setTwoFA;
            sendHandler = () => props.twoFactorAuth(props.twoFactorToken, 
                obj.content.token.value.toString().replace(/\s/g, ''), 
                !!obj.content.save.value);
            break;
        default:
            headLine = "Sign in";
            buttonText = "Sign in";
            switchText = "Forgot password?";
            forgotSwitch = "#forgotpassword";
            obj = signin;
            sendHandler = () => props.auth(signin.content.email.value.toString(), signin.content.password.value.toString());
            setObj = setSignin;
    }

    //handlers

    const changeHandler = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> | CustomEvent): void => {
        setObj(updateForm(obj, e));
    }

    const keyupHandler = (e: KeyboardEvent<HTMLTextAreaElement>): void => {
        e.preventDefault();
        e.stopPropagation();
    }

    const submitHandler = (e: FormEvent) => {
        e.preventDefault();
        sendHandler();
    }

    if (props.loading) {
        elementToDisplay = (
            <div style={{ height: "300px", display: "flex", justifyContent: "center", alignItems: "center", position: "relative" }}>
                <Spinner />
            </div>
        );
    }
    else {
        elementToDisplay = (
            <Fragment>
                <H1 data-testid="headLine">{headLine}</H1>
                {   
                    Object.keys(obj.content).map((k: string) =>
                        <Input
                            elementConfig={obj.content[k].elementConfig}
                            changed={changeHandler}
                            key={obj.content[k].name}
                            touched={obj.content[k].touched}
                            keyup={keyupHandler}
                            shouldValidate={obj.content[k].shouldValidate}
                            invalid={!obj.content[k].valid}
                            value={obj.content[k].value}
                            name={obj.content[k].name}
                            id={obj.content[k].name}
                            elementType={obj.content[k].elementType}
                            desc={obj.content[k].desc || ""}
                        />
                    )}
                {obj.content && obj.message ? <Status form={obj} /> : ""}
                <br/>
                {mode === "signin" || mode === "forgot" ? <NavLink to={forgotSwitch}><SwitchMode>{switchText}</SwitchMode></NavLink> : null}
                {mode === "2fa" && !props.smsSent ? <SwitchMode clicked={() => props.sendSms(props.twoFactorToken)}>{switchText}</SwitchMode> : null}
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <Button
                        type="submit"
                        passStyle={{ margin: props.smsSent ? "40px auto 30px auto" : "15px auto 30px auto"}}
                        name="signin form button"
                        disabled={!obj.isValid}>
                        {buttonText}
                    </Button>
                </div>
            </Fragment>
        );
    }

    return (
        <div>
            <Wrapper>
                <LoginContainer>
                    <form data-testid="form" onSubmit={submitHandler} style={{ width: "90%", margin: "auto" }}>
                        {elementToDisplay}
                    </form>
                </LoginContainer>
            </Wrapper>
        </div>
    );
}

export const TestAuth: React.FC<any> = Auth;


const mapStateToProps = (state: any) => {
    return {
        error: state.auth.error,
        loading: state.auth.loading,
        notice: state.auth.notice,
        smsSent: state.auth.smsSent,
        twoFactorToken: state.auth.twoFactorToken
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        auth: (email: string, password: string) => dispatch(actions.auth(email, password)),
        clearError: () => dispatch(actions.clearError()),
        forgotPassword: (email: string) => dispatch(actions.forgotPassword(email)),
        resetNotice: () => dispatch(actions.resetNotice()),
        resetPassword: (password: string, repeatPassword: string, resetToken: string) => dispatch(
            actions.resetPassword(password, repeatPassword, resetToken)),
        sendSms: (token: string) => dispatch(actions.sendSms(token)),
        twoFactorAuth: (token: string, code: string, save: boolean) => dispatch(actions.twoFactorAuth(token, code, save))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Auth));