import { Ce, Config, RulesInterface } from '../../shared/constants';

export interface BaseLoginFormStructure {
    content: ContentInterface;
    isValid: boolean;
    error: Ce | boolean;
    message: string;
}

export interface ContentInterface {
    [key: string]: FieldStructure
}

export interface FieldStructure {
    value: string | boolean | number;
    valid: boolean;
    touched: boolean;
    name: string;
    shouldValidate: boolean;
    rules: RulesInterface;
    elementConfig: Config;
    elementType: string;
    expand?: boolean;
    offset?: number;
    desc?: string;
}

export interface AuthProps {
    auth: (username: string, password: string) => void;
    clearError: () => void;
    sendSms: (token: string) => void;
    forgotPassword: (email: string) => void;
    twoFactorAuth: (token: string, code: string, save: boolean) => void;
    resetNotice: () => void;
    resetPassword: (password: string, repeatPassword: string, resetToken: string) => void;
    error: Ce;
    loading: Boolean;
    history: any;
    notice: string;
    smsSent: boolean;
    twoFactorToken: string;
}