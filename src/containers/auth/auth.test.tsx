import React from 'react';
import ReactDOM from 'react-dom';
import {render, cleanup} from '@testing-library/react';
import testrender from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';

//local imports
import {TestAuth} from './auth';

describe('Auth component', () => {

    //setup
    afterEach(cleanup);
    document.cookie = "reset=true";

    //standard configs
    const standard = <BrowserRouter><TestAuth history={{location: ""}}></TestAuth></BrowserRouter>;
    const resethash = <BrowserRouter><TestAuth history={{location: {hash: "#reset"}}}></TestAuth></BrowserRouter>;
    const forgothash = <BrowserRouter><TestAuth history={{location: {hash: "#forgotpassword"}}}></TestAuth></BrowserRouter>;
    const twoFA = <BrowserRouter><TestAuth history={{location: ""}} twoFactorToken="gdsgsdg"></TestAuth></BrowserRouter>;

    it("should render without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<BrowserRouter><TestAuth history={{location: ""}}/></BrowserRouter>, div)
    });

    it("renders h1 with text signin when mode is signin", () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('headLine')).toHaveTextContent("Sign in");
    });

    it("renders h1 with text Forgot password when mode is forgot password", () => {
        const {getByTestId} = render(forgothash);
        expect(getByTestId('headLine')).toHaveTextContent("Forgot Password");
    });
    
    it("renders h1 with text Reset password when mode is reset password", () => {
        const {getByTestId} = render(resethash);
        expect(getByTestId('headLine')).toHaveTextContent("Reset Password");
    });

    it("renders h1 with text Two-Factor Authentication when mode is 2fa", () => {
        const {getByTestId} = render(twoFA);
        expect(getByTestId('headLine')).toHaveTextContent("Two-Factor Authentication");
    });

    it("renders spinner when props.loading is true", () => {
        const {getByTestId} = render(<BrowserRouter><TestAuth history={{location: ""}} loading={true}></TestAuth></BrowserRouter>);
        expect(getByTestId('form').innerHTML).toMatch(/data-testid=\"spinner\"/);
    });

    it("does not render the spinner when props.loading is false", () => {
        const {getByTestId} = render(<BrowserRouter><TestAuth history={{location: ""}} loading={false}></TestAuth></BrowserRouter>);
        expect(getByTestId('form').innerHTML).not.toMatch(/data-testid=\"spinner\"/);
    });

    it("renders switchmode component when mode is signin", () => {
        const {getByTestId} = render(standard);
        expect(getByTestId('form').innerHTML).toMatch(/data-testid=\"smalltext\"/);
    });

    it("renders switchmode component when mode is forgot password", () => {
        const {getByTestId} = render(forgothash);
        expect(getByTestId('form').innerHTML).toMatch(/data-testid=\"smalltext\"/);
    });

    it("renders switchmode component when mode is 2fa", () => {
        const {getByTestId} = render(twoFA);
        expect(getByTestId('form').innerHTML).toMatch(/data-testid=\"smalltext\"/);
    });

    it("does not render component when mode is reset password", () => {
        const {getByTestId} = render(resethash);
        expect(getByTestId('form').innerHTML).not.toMatch(/data-testid=\"smalltext\"/);
    });
    it("should match snapshot", () => {
        const instance = testrender.create(standard);
        expect(instance).toMatchSnapshot();
    });

    
});