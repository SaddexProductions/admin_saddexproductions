//npm modules
import React, {Fragment, useEffect } from 'react';
import { Route, Switch, withRouter, Redirect, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';

//local modules
import './App.css';
import Auth from './containers/auth/auth';
import Header from './components/header/header';
import Items from './containers/items/items';
import Pages from './containers/pages/pages';
import * as actions from './store/actions/index';
import Spinner from './components/UI/spinner/spinner';

const App: React.FC<StateProps & DispatchProps> = props => {

  //use history
  const history = useHistory();

  //useeffect
  useEffect(() => {

    if(props.isAuth) {
      history.replace(props.authRedirectPath);
      let d = new Date();
      d.setTime(d.getTime() + 30 * 24 * 60 * 60 * 1000); //Signin cookie valid for 30 days
      const expires = "expires=" + d.toUTCString();
      document.cookie = "logintoken =" + props.token + ";" + expires + ";path=/";
    }
    else {
      if(props.firstCheck) props.onAutoSignup();

      let path: string = history.location.pathname + history.location.hash + history.location.search;

      if(history.location.pathname === "/login") path = "/";

      if(props.firstCheck) props.setAuthRedirectPath(path);

      if(props.authRedirectPath.substr(1).length > 0 && !history.location.search && history.location.pathname === "/login") {
        history.replace("?redirect=" + props.authRedirectPath.substr(1));
      } else if(history.location.search && history.location.search.indexOf("redirect=") > -1) {

        let str = history.location.search.split("redirect=")[1];

        if(str) {
          str = str.split("&")[0];
          props.setAuthRedirectPath(str);
        } else {
          history.replace('/login');
        }
      }
    }

  }, [props.isAuth, props.authRedirectPath, props.token, history, props.setAuthRedirectPath]);

  //variables
  let routes: JSX.Element | null = null;
  let routesOrLoader: JSX.Element = (<div 
  style={{display: "inline-block", width: "100%", marginTop: "70px", height: "calc(100vh - 70px)"}}>
    <div style={{display: "flex", alignItems: "center", justifyContent: "center", height: "90%"}}>
      <Spinner white/>
    </div>
  </div>);

  if(props.isAuth) {
    routes = (
      <Switch>
        <Route path="/items" component={Items} />
        <Route path="/" component={Pages} />
        <Redirect to="/" />
      </Switch>
    );
  }
  else {
    routes = (<Switch>
      <Route path="/login" component={Auth} />
      <Redirect to="/login"/>
    </Switch>);
  }

  if(!props.firstCheck) routesOrLoader = (
    <Fragment>
      <Header menuOpen={props.menuOpen} closeMenu={props.closeMenu} toggleMenu={props.toggleMenu}/>
      {routes}
    </Fragment>
  );

  return (
    <Fragment>
      {routesOrLoader}
    </Fragment>
  );
}

interface DispatchProps {
  closeMenu: () => void;
  toggleMenu: () => void;
  onAutoSignup: () => void;
  setAuthRedirectPath: (path: string) => void;
}

interface StateProps {
  authRedirectPath: string;
  isAuth: boolean;
  firstCheck: boolean;
  menuOpen: boolean;
  token: string | null;
}

const mapStateToProps = (state: any) => {
  const obj: StateProps = {
    authRedirectPath: state.auth.authRedirectPath,
    firstCheck: state.auth.firstCheck,
    menuOpen: state.globalUI.menuOpen,
    isAuth: state.auth.token !== null,
    token: state.auth.token
  }
  return obj;
}

const mapDispatchToProps = (dispatch: any) => {
  const obj: DispatchProps = {
    closeMenu: () => dispatch(actions.closeMenu()),
    toggleMenu: () => dispatch(actions.toggleMenu()),
    onAutoSignup: () => dispatch(actions.authCheckState()),
    setAuthRedirectPath:  (path: string) => dispatch(actions.setAuthRedirectPath(path))
  }
  return obj;
}

export default withRouter(connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(App));
