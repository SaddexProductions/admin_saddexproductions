//npm modules
import { ChangeEvent } from 'react';

//local imports
import {Ce, CustomEvent, RulesInterface} from'./constants';
import {BaseLoginFormStructure, FieldStructure, ContentInterface} from '../containers/auth/localInterfaces';

//updates objects through object destructuring
export const updateObject = (oldObject: Object, updatedProperties: any) =>{
    return {
        ...oldObject,
        ...updatedProperties
    }
}

//validation function, very reusable
export const checkValid = (val: string | number, rules: RulesInterface, parentClass: BaseLoginFormStructure) =>{
    let isValid: boolean = true;

    if(rules.required){
        isValid = val.toString().trim() !== "" && isValid;
    }
    if (rules.trim && typeof val === 'string') val = val.replace(/\s/g, '');

    if(typeof val === "string" && rules.minLength){
        isValid = val.length >= rules.minLength && isValid;
    }
    if(typeof val === "string" && rules.maxLength){
        isValid = val.length <= rules.maxLength && isValid;
    }

    if(rules.isEmail){
        const pattern: RegExp =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        isValid = typeof val === "string" && pattern.test(val) && isValid;
    }
    if(rules.isNumeric) {
        const pattern: RegExp = /^\d+$/
        isValid = pattern.test(val.toString()) && isValid
    }
    if ((rules.regex && rules.required && typeof val === "string") || (typeof val === "string" && rules.regex && val.length > 0)) {
        isValid = rules.regex.test(val) && isValid
    }
    if (rules.mustMatch) {
        isValid = val === parentClass.content.password.value && isValid
    }
    return isValid;
}

export const updateForm = (obj: BaseLoginFormStructure, e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> | CustomEvent) => {

    let str = e.target.value;

    if (obj.content[e.target.id].expand && typeof obj.content[e.target.id].value === "string") {
        str = splitNumber(e.target.value.toString(), {...obj.content[e.target.id], value: obj.content[e.target.id].value.toString()});
    }

    const upFormEl: FieldStructure = updateObject(obj.content[e.target.id], {
        value: str,
        valid: obj.content[e.target.id].rules && typeof e.target.value !== "boolean" ? checkValid(e.target.value, obj.content[e.target.id].rules, obj) : true,
        touched: true
    });
    
    const updatedForm: ContentInterface = updateObject(obj.content, {
        [e.target.id]: upFormEl
    });

    if(e.target.id === "password" && obj.content["repeat-Password"] !== undefined) {
        obj.content["repeat-Password"].valid = 
        checkValid(obj.content["repeat-Password"].value.toString(), obj.content["repeat-Password"].rules, {
            ...obj,
            content: {
                ...obj.content,
                password: upFormEl
            }
        });
    }

    updatedForm[e.target.id] = upFormEl;

    return validate(obj, updatedForm);
}

//checks if all fields are set as valid and if so, returns the full form as valid
export const validate = (state: BaseLoginFormStructure, updatedForm: ContentInterface) =>{
    let isValid: boolean = true;
    for (let inId in updatedForm) {
        isValid = updatedForm[inId].valid && isValid;
    }

    return updateObject(state, { content: updatedForm, isValid } );
}

//converts error from backend into the appropriate message
export const convertError = (err: Ce): string => {
    if (err.response && err.response.data && err.response.data.indexOf("html") === -1) {
        return err.response.data;
    }
    else {
        return err.message;
    }
}

//auto-formats a number
export const splitNumber = (inputString: string, obj: {offset?: number; value: string}) => {
    let str: string | string[];
    if (obj.value.length <= inputString.length) {
      str = inputString.replace(/\s/g, '').split('');
      const objOffset: number = obj.offset || 0;
      for (let i = 0; i < str.length; i++) {
        if ((i + objOffset) % 4 === 0 && str[i] !== " " && i !== 0) {
          str.splice(i, 0, ' ');
        }
      }
    }
    else {
      str = inputString.split('');
      if (str[str.length - 1] === ' ') str.splice(str.length - 1, 1);
    }
    str = str.join('');
  
    return str;
  }