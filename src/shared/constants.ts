//npm modules
import {AxiosResponse} from 'axios';

export interface Ce {
    response: AxiosResponse;
    message: string;
}

export interface Config {
    type: "email" | "password" | "text" | "number" | "checkbox" | "radio";
    placeholder?: string;
    maxLength?: number;
    options?: {
        value: string;
        displayValue: string;
    }[]
}

export interface RulesInterface {
    required: boolean;
    isNumeric?: boolean;
    isEmail?: boolean;
    minLength?: number;
    maxLength?: number;
    trim?: boolean;
    regex?: RegExp;
    mustMatch?: boolean;
}

export interface User {
    _id: string;
    email: string;
    twoFactorAuth: {
        cellphone: string;
        countryCode: string;
    }
}

export interface CustomEvent {
    target: {
        value: boolean | string | number;
        id: string;
    }
}