import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';
import {ActionModel, AuthModel} from '../actions/baseType';
import { User } from '../../shared/constants';

export interface StateStructure {
    authRedirectPath: string;
    error: null | Error;
    firstCheck: boolean;
    loading: boolean;
    notice: null | string;
    smsSent: boolean;
    token: null | string;
    twoFactorToken: null | string;
    user: null | User;
}

const initialState: StateStructure = {
    token: null, //production default is null
    twoFactorToken: null,
    user: null,
    error: null,
    firstCheck: true,
    notice: null,
    loading: false,
    authRedirectPath: '/',
    smsSent: false
}

const authStart = (state: StateStructure): Object =>{
    return updateObject(state, {
        error: null, 
        loading: true,
        notice: null
    });
}

const authSuccess = (state: StateStructure, action: ActionModel & AuthModel): Object =>{
    return updateObject(state, {
        token: action.token,
        user: action.user,
        error: null,
        loading: false,
        twoFactorToken: null,
        firstCheck: false
    });
}

const authFail = (state: StateStructure, action: ActionModel & AuthModel): Object =>{
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}

const authLogout = (state: StateStructure): Object => {
    return updateObject(state, {
        token: null,
        user: null,
        firstCheck: false
    });
}

const redirTwoFa = (state: StateStructure, action: ActionModel & AuthModel): Object => {
    return updateObject(state, {
        twoFactorToken: action.token,
        loading: false,
        error: null,
        smsSent: false
    });
}

const resetNoticeFunction = (state: StateStructure) => {
    return updateObject(state, {
        notice: null
    });
}

const smsSuccessfulFunction = (state: StateStructure): Object => {
    return updateObject(state, {
        notice: "Sms successfully sent!",
        loading: false,
        error: null,
        smsSent: true
    })
}

const forgotPasswordSuccessFunction = (state: StateStructure, action: ActionModel & AuthModel): Object => {
    return updateObject(state, {
        notice: action.message,
        loading: false,
        error: null
    })
}

const clearErrorFunction = (state: StateStructure): Object => {
    return updateObject(state, {
        error: null,
        notice: null
    })
}

const setPath = (state: StateStructure, action: ActionModel & AuthModel): Object => {
    return updateObject(state, {
        authRedirectPath: action.path
    });
}

const reducer = (state: StateStructure = initialState, action: ActionModel & AuthModel): Object =>{
    switch(action.type){
        case actionTypes.AUTH_START: return authStart(state);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_LOGOUT: return authLogout(state);
        case actionTypes.SMS_SUCCESSFUL: return smsSuccessfulFunction(state);
        case actionTypes.FORGOT_PASSWORD_SUCCESS: return forgotPasswordSuccessFunction(state, action);
        case actionTypes.REDIRECT_TWO_FACTOR_AUTH: return redirTwoFa(state, action);
        case actionTypes.RESET_NOTICE: return resetNoticeFunction(state);
        case actionTypes.CLEAR_ERROR: return clearErrorFunction(state);
        case actionTypes.SET_AUTH_REDIRECT_PATH: return setPath(state, action);
        default: return state;
    }
}

export default reducer;