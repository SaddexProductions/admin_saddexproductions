import * as actionTypes from '../actions/actionTypes';
import {updateObject} from '../../shared/utility';
import {ActionModel} from '../actions/baseType';

export interface StateStructure {
    menuOpen: boolean;
    modalOpen: boolean;
    langMenu: boolean;
}

const initialState: StateStructure = {
    menuOpen: false,
    modalOpen: false,
    langMenu: false
}

const toggleMenu = (state: StateStructure): Object => {
    let langMenuOpen = state.langMenu;

    if(state.menuOpen) langMenuOpen = false;

    return updateObject(state, {
        menuOpen: !state.menuOpen,
        langMenu: langMenuOpen
    });
}

const closeMenu = (state: StateStructure): Object => {
    return updateObject(state, {
        menuOpen: false
    });
}

const setSize = (state: StateStructure): Object => {
    return updateObject(state, {
        windowWidth: window.innerWidth
    });
}

const openModal = (state: StateStructure): Object => {
    return updateObject(state, {
        modalOpen: true
    });
}

const closeModal = (state: StateStructure): Object => {
    return updateObject(state, {
        modalOpen: false
    });
}

const toggleLangMenu = (state: StateStructure): Object => {
    return updateObject(state, {
        langMenu: !state.langMenu
    });
}

const closeLangMenu = (state: StateStructure): Object => {
    return updateObject(state,{
        langMenu: false
    });
}

const reducer = (state: StateStructure = initialState, action: ActionModel) =>{
    switch(action.type){
        case(actionTypes.TOGGLE_MENU): return toggleMenu(state);
        case(actionTypes.CLOSE_MENU): return closeMenu(state);
        case(actionTypes.TOGGLE_LANGUAGE_MENU): return toggleLangMenu(state);
        case(actionTypes.CLOSE_LANGUAGE_MENU): return closeLangMenu(state);
        case(actionTypes.SET_SIZE): return setSize(state);
        case(actionTypes.MODAL_OPEN): return openModal(state);
        case(actionTypes.MODAL_CLOSED): return closeModal(state);
        default: return state;
    }
}

export default reducer;