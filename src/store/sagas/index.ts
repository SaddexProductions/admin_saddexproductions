import { takeLatest } from 'redux-saga/effects';

//local imports
import {authCheckStateSaga, authUserSaga, twoFASaga, sendSmsSaga, forgotPasswordSaga, resetPasswordSaga, logoutSaga} from './auth';
import * as actionTypes from '../actions/actionTypes';

export function* watchAuth (){
    yield takeLatest(actionTypes.AUTH_USER, authUserSaga);
    yield takeLatest(actionTypes.AUTH_CHECK_INITIAL_STATE, authCheckStateSaga);
    yield takeLatest(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);
    yield takeLatest(actionTypes.TWO_FACTOR_AUTH, twoFASaga);
    yield takeLatest(actionTypes.SEND_SMS, sendSmsSaga);
    yield takeLatest(actionTypes.FORGOT_PASSWORD, forgotPasswordSaga);
    yield takeLatest(actionTypes.RESET_PASSWORD, resetPasswordSaga);
}