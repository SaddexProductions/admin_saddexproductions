//npm modules
import { put } from 'redux-saga/effects';
import axios, { AxiosResponse } from 'axios';

//local modules
import * as actions from '../actions/index';
import {ActionModel, AuthModel} from '../actions/baseType';

//base url path
const basePath: string = "http://localhost:3000/api/login";

export function* logoutSaga(_: ActionModel & AuthModel) {
    document.cookie = "logintoken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    yield put(actions.logoutSucceed());
}

export function* authUserSaga(action: ActionModel & AuthModel) {
    yield put(actions.authStart());
    const authData = {
        email: action.email,
        password: action.password
    }
    let headers = {};

    if(localStorage.getItem("trusted")) headers = {
        "x-trusted": localStorage.getItem("trusted")
    };

    try {
        const res: AxiosResponse = yield axios.post(basePath, authData, {
            headers
        });
        if (res.data.twoFactorAuth) {
            yield put(actions.redirectToTwoFA(res.data.jwt));
        } else {
            yield put(actions.authSuccess(res.data.jwt, res.data.user));
        }
    }

    catch (err) {
        yield put(actions.authFail(err));
    }
}

export function* twoFASaga(action: ActionModel & AuthModel) {
    yield put(actions.authStart());
    const authData = {
        code: action.code,
        save: action.save
    }
    try {
        const res: AxiosResponse = yield axios.post(basePath + '/twofactorauth', authData, {
            headers: {
                'x-auth-token': action.token
            }
        });
        if(res.data && res.data.trusted) {
            localStorage.setItem("trusted", res.data.trusted);
        }
        yield put(actions.authSuccess(res.data.jwt, res.data.user));
    }

    catch (err) {
        yield put(actions.authFail(err));
    }
}

export function* forgotPasswordSaga(action: ActionModel & AuthModel) {
    yield put(actions.authStart());

    try {
        const res: AxiosResponse = yield axios.post('http://localhost:3000/api/users/resetpassword', {email: action.email});
        yield put(actions.forgotPasswordSuccess(res.data));
    }

    catch (err) {
        yield put(actions.authFail(err));
    }
}

export function* resetPasswordSaga(action: ActionModel & AuthModel) {
    yield put(actions.authStart());

    try {
        const res: AxiosResponse = yield axios.post('http://localhost:3000/api/users/newpassword', {
            password: action.password,
            repeatPassword: action.repeatPassword
        }, {
            headers: {
                'x-reset-token': action.resetToken
            }
        })
        document.cookie = "reset=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        yield put(actions.forgotPasswordSuccess(res.data));
    }

    catch (err) {
        yield put(actions.authFail(err));
    }
}

export function* sendSmsSaga(action: ActionModel & AuthModel) {
    yield put(actions.authStart());

    try {
        yield axios.get(basePath + '/requestsms', {
            headers: {
                'x-auth-token': action.token
            }
        });
        yield put(actions.smsSuccessful());
    }

    catch (err) {
        yield put(actions.authFail(err));
    }
}

export function* authCheckStateSaga(_: ActionModel & AuthModel) {
    const cookie: string = document.cookie;
    if(!cookie || cookie.indexOf("logintoken") < 0) {
        yield put(actions.logout());
        return;
    }

    let cook: string = "";
    let value: string = "; " + cookie;
    let parts: string[] = value.split("; logintoken=");
    if (parts.length > 1) cook = parts[parts.length-1].split(";")[0];

    if (!cook) {
        yield put(actions.logout());
        return;
    }
    try {
        const res: AxiosResponse = yield axios.get(basePath, {
            headers: {
                'x-auth-token': cook
            }
        });
        yield put(actions.authSuccess(cook, res.data.user));
    }
    catch(_) {
        yield put(actions.logout());
    }
}