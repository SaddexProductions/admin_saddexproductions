//This file contains auth-related Redux actions

import * as actionTypes from './actionTypes';
import { ActionModel, AuthModel } from './baseType';
import { User } from '../../shared/constants';

export const auth = (email: string, password: string): ActionModel & AuthModel =>{
    return {
        type: actionTypes.AUTH_USER,
        email,
        password
    }
}

export const authCheckState = (): ActionModel => {
    return {
        type: actionTypes.AUTH_CHECK_INITIAL_STATE
    }
}

export const authFail = (error: Error): ActionModel & AuthModel => {
    return {
        type: actionTypes.AUTH_FAIL,
        error
    }
}

export const authStart = (): ActionModel => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token: string, user: User): ActionModel & AuthModel =>{
    return {
        type: actionTypes.AUTH_SUCCESS,
        token,
        user
    }
}

export const clearError = (): ActionModel => {
    return {
        type: actionTypes.CLEAR_ERROR
    }
}

export const forgotPassword = (email: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.FORGOT_PASSWORD,
        email
    }
}

export const forgotPasswordSuccess = (message: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.FORGOT_PASSWORD_SUCCESS,
        message
    }
}

export const logout = (): ActionModel =>{
    return {
        type: actionTypes.AUTH_INITIATE_LOGOUT
    }
}

export const logoutSucceed = (): ActionModel =>{
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const redirectToTwoFA = (twoFactorToken: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.REDIRECT_TWO_FACTOR_AUTH,
        token: twoFactorToken
    }
}

export const resetNotice = (): ActionModel => {
    return {
        type: actionTypes.RESET_NOTICE
    }
}

export const sendSms = (twoFactorToken: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.SEND_SMS,
        token: twoFactorToken
    }
}

export const setAuthRedirectPath = (path: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path
    }
}

export const smsSuccessful = (): ActionModel => {
    return {
        type: actionTypes.SMS_SUCCESSFUL
    }
}

export const resetPassword = (password: string, repeatPassword: string, resetToken: string): ActionModel & AuthModel => {
    return {
        type: actionTypes.RESET_PASSWORD,
        password,
        repeatPassword,
        resetToken
    }
}

export const twoFactorAuth = (token: string, code: string, save: boolean): ActionModel & AuthModel => {
    return {
        type: actionTypes.TWO_FACTOR_AUTH,
        token,
        code,
        save
    }
}