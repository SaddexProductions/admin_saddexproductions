//This file contains redux actions related to global UI state

import * as actionTypes from './actionTypes';
import { ActionModel } from './baseType';

type Action = () => ActionModel;

export const toggleMenu: Action = () => {
    return {
        type: actionTypes.TOGGLE_MENU
    }
}

export const closeMenu: Action = () => {
    return {
        type: actionTypes.CLOSE_MENU
    }
}

export const toggleLangMenu: Action = () => {
    return {
        type: actionTypes.TOGGLE_LANGUAGE_MENU
    }
}

export const closeLangMenu: Action = () => {
    return {
        type: actionTypes.CLOSE_LANGUAGE_MENU
    }
}

export const setSize: Action = () => {
    return {
        type: actionTypes.SET_SIZE
    }
}

export const modalOpen: Action = () => {
    return {
        type: actionTypes.MODAL_OPEN
    }
}

export const modalClose: Action = () => {
    return {
        type: actionTypes.MODAL_CLOSED
    }
}