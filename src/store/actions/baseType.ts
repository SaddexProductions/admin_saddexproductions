//The base interfaces for actions

import { User } from "../../shared/constants";

export interface ActionModel {
    type: string
}

export interface AuthModel {
    token?: string;
    code?: string;
    user?: User;
    error?: Error;
    email?: string;
    message?: string;
    password?: string;
    path?: string;
    repeatPassword?: string;
    save?: boolean;
    resetToken?: string;
}