//This is a collection of all available redux actions

export const TOGGLE_MENU: string = "TOGGLE_MENU";
export const CLOSE_MENU: string = "CLOSE_MENU";
export const TOGGLE_LANGUAGE_MENU: string = "TOGGLE_LANGUAGE_MENU";
export const CLOSE_LANGUAGE_MENU: string = "CLOSE_LANGUAGE_MENU";
export const SET_SIZE: string = "SET_SIZE";
export const MODAL_OPEN: string = "MODAL_OPEN";
export const MODAL_CLOSED: string = "MODAL_CLOSED";

export const AUTH_CHECK_INITIAL_STATE: string = "AUTH_CHECK_INITIAL_STATE";
export const AUTH_INITIATE_LOGOUT: string = "AUTH_INITIATE_LOGOUT";
export const AUTH_LOGOUT: string = "AUTH_LOGOUT";
export const AUTH_USER: string = "AUTH_USER";
export const AUTH_START: string = "AUTH_START";
export const AUTH_SUCCESS: string = "AUTH_SUCCESS";
export const AUTH_FAIL: string = "AUTH_FAIL";
export const CLEAR_ERROR: string = "CLEAR_ERROR";
export const REDIRECT_TWO_FACTOR_AUTH: string = "REDIRECT_TWO_FACTOR_AUTH";
export const FORGOT_PASSWORD: string = "FORGOT_PASSWORD";
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const RESET_NOTICE: string = "RESET_NOTICE";
export const RESET_PASSWORD: string = "RESET_PASSWORD";
export const SEND_SMS: string = "SEND_SMS";
export const SET_AUTH_REDIRECT_PATH = "SET_AUTH_REDIRECT_PATH";
export const SMS_SUCCESSFUL: string = "SMS_SUCCESSFUL";
export const TWO_FACTOR_AUTH: string = "TWO_FACTOR_AUTH";
